import Logger from "./logger"
import Server from "./server"

const app = new Server()

app.use(Logger)

app.get("/", (_, res) => res.text("Welcome!"))
app.get("/hello/:name", (req, res) => res.text(`Hello, ${req.params.name}!`))

app.run()

process.on("SIGINT", () => {
  console.log("Received SIGINT. Attempting shutdown...")
  app.stop()
})