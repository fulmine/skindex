import type Context from "./server/context"
import Middleware from "./server/middleware"
import type ServerRequest from "./server/request"
import type ServerResponse from "./server/response"

export default class Logger extends Middleware {
  beforeFn = (() => {})
  afterFn = (req: ServerRequest, res: ServerResponse) => {
    this.endTime = new Date()
    const miliseconds = this.endTime.getTime() - this.startTime.getTime()

    console.log(`${req.request.method} ${new URL(req.request.url).pathname}: ${res.status} (${miliseconds}ms elapsed)`)
  }

  private startTime: Date
  private endTime: Date|null = null

  constructor() {
    super()
    this.startTime = new Date()
  }
}