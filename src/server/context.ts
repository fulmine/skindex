export interface ContextOptions {
    cors: string|null
}

export default class Context {
    readonly corsPolicy: string|null
    
    constructor(options: ContextOptions) {
        this.corsPolicy = options.cors
    }
}