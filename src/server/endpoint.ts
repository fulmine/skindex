import { match, type MatchFunction } from "path-to-regexp"
import type { HttpMethod } from "."
import ServerResponse from "./response"
import type ServerRequest from "./request"
import type Context from "./context"

type HandleFn = (req: ServerRequest, res: ServerResponse, ctx: Context) => Promise<Response>|Response

interface EndpointOptions {
  cors?: string|null
}

export default class Endpoint {
  private httpMethod: HttpMethod
  private matchFn: MatchFunction
  private handleFn: HandleFn
  
  readonly corsPolicy: string|null = null

  constructor(method: HttpMethod, path: string, handle: HandleFn, options?: EndpointOptions) {
    this.httpMethod = method
    this.matchFn = match(path, { decode: decodeURIComponent }) 
    this.handleFn = handle

    if (options?.cors) this.corsPolicy = options.cors

    return this
  }

  matchRoute(path: string) {
    const match = this.matchFn(path)
    if (match) return match
    else return null
  }

  async handle(req: ServerRequest, res: ServerResponse, ctx: Context) {
    return await this.handleFn(req, res, ctx)
  }

  checkMethod(httpMethod: HttpMethod) {
    return httpMethod = this.httpMethod
  }
}