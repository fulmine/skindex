import type { VNode } from "preact"
import Endpoint from "./endpoint"
import ServerResponse from "./response"
import { type Server as BunServer } from "bun"
import Middleware from "./middleware"
import ServerRequest from "./request"
import Context from "./context"

/**
 * Allowed HTTP Methods
 */
export type HttpMethod = "GET"|"POST"|"PATCH"|"DELETE"

export interface ServerOptions {
  port?: number,
  notFoundPage?: VNode
  development?: boolean,
  cors?: string
}

/**
 * A very silly HTTP server
 */
export default class Server {
  private server: BunServer|null = null
  private port: number = 3000
  private notFoundPage: VNode|null = null
  private development: boolean = false
  private corsPolicy: string|null = null
  
  private endpoints: Endpoint[] = []
  private middlewares: (typeof Middleware)[] = []

  /**
   * Creates an instance of Server
   * @param {ServerOptions} options
   * Server options
   */
  constructor(options?: ServerOptions) {
    if (options?.development) this.development = options.development
    if (options?.notFoundPage) this.notFoundPage = options.notFoundPage
    if (options?.port) this.port = options.port
    if (options?.cors) this.corsPolicy = options.cors
  }

  use(middleware: typeof Middleware) {
    this.middlewares.push(middleware)
    return this
  }

  /**
   * Creates a GET endpoint at the specified path
   * @param path 
   * Path used for this endpoint (according to https://github.com/pillarjs/path-to-regexp)
   * @param handle 
   * Function ran whenever this endpoint gets called.
   * @returns
   */
  get(path: string, handle: (req: ServerRequest, res: ServerResponse, ctx: Context) => Promise<Response>|Response) {
    this.endpoints.push(new Endpoint("GET", path, handle))
    return this
  }

  /**
   * Starts the server and listens on the specified port.
   * @param port
   * Port used for the server.
   * This overrides the port specified in the constructor options
   * @returns 
   */
  run(port?: number) {
    if (port) this.port = port

    console.log(`Starting server on port ${this.port}!\n`)
    
    const server = this
    this.server = Bun.serve({
      development: server.development,
      port: port,
      async fetch(request) {
        const url = new URL(request.url)

        // loop through every endpoint
        for (const endpoint of server.endpoints) {
          const match = endpoint.matchRoute(url.pathname) 
          if (!match || !endpoint.checkMethod(request.method as HttpMethod)) continue

          const ctx = new Context({
            cors: endpoint.corsPolicy || server.corsPolicy
          })
          const serverRes = new ServerResponse(ctx)
          const serverReq = new ServerRequest(request, match.params as Record<string, string>)

          const callback = () => endpoint.handle(serverReq, serverRes, ctx)
          const response = await server.handleRequest(serverReq, serverRes, ctx, callback)

          return response
        }

        // no page found, show the 404 page
        const ctx = new Context({
          cors: server.corsPolicy
        })
        const serverReq = new ServerRequest(request, {})
        const serverRes = new ServerResponse(ctx)

        const callback = () => server.returnNotFound(serverRes)
        const response = await server.handleRequest(serverReq, serverRes, ctx, callback)

        return response
      }
    })

    return this
  }

  /**
   * Stops the server if running.
   */
  stop() {
    if (this.server) {
      this.server.stop()
      console.log("Server shutdown complete!")
    } else {
      console.log("Server was not started to begin with.")
    }
  }

  /**
   * Returns the error page.
   * @param response 
   * @returns 
   */
  private returnNotFound(response: ServerResponse) {
    response.status = 404
    if (!this.notFoundPage) {
      return response.text("404: Not Found")
    }
    else return response.jsx(this.notFoundPage)
  }

  private async handleRequest(request: ServerRequest, serverRes: ServerResponse, ctx: Context, callbackFn: () => Promise<Response>|Response) {
    // handle all middleware functions before the request processing
    const m: Middleware[] = []
    for (const middleware of this.middlewares) {
      const mw = new(middleware)()
      m.push(mw)
      mw.handleBefore(request, serverRes, ctx)
    }
    
    // handle the request itself
    const response = await callbackFn()
    
    // handle all middleware functions after the request
    for (const mw of m) {
      mw.handleAfter(request, serverRes, ctx)
    }

    return response
  }
}