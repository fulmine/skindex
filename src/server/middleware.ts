import type Context from "./context"
import type ServerRequest from "./request"
import ServerResponse from "./response"

/**
 * Sloppy middleware that runs a function before and after every request.
 */
export default class Middleware {
  protected beforeFn: (req: ServerRequest, res: ServerResponse, ctx: Context) => void|Promise<void> = (() => {})
  protected afterFn: (req: ServerRequest, res: ServerResponse, ctx: Context) => void|Promise<void> = (() => {})

  constructor() {}

  async handleBefore(req: ServerRequest, res: ServerResponse, ctx: Context) {
    return await this.beforeFn(req, res, ctx)
  }

  async handleAfter(req: ServerRequest, res: ServerResponse, ctx: Context) {
    return await this.afterFn(req, res, ctx)
  }
}