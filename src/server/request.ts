export default class ServerRequest {
    readonly request: Request
    readonly params: Record<string, string>

    constructor(request: Request, params: Record<string, string>) {
        this.request = request
        this.params = params
    }
}