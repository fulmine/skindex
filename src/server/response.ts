import type { VNode } from "preact"
import renderToString from "preact-render-to-string"
import type Context from "./context"

interface RespOptions {
  headers?: Record<string, string>
}

export default class ServerResponse {
  status: number = 200
  private headers = {
    "X-Content-Type-Options": "nosniff"
  }

  constructor(ctx: Context) {
    if (ctx.corsPolicy) {
      this.headers = {...this.headers, ...{
        "Access-Control-Allow-Origin": ctx.corsPolicy
      }}
    }
  }

  text(body: string) {
    return this.resp(body, {
      headers: {
        "Content-Type": "text/plain; charset=utf-8",
      }
    })
  }

  json(body: Record<string, any>|[]) {
    return this.resp(JSON.stringify(body), {
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      }
    })
  }

  jsx(body: VNode) {
    return this.resp(renderToString(body), {
      headers: {
        "Content-Type": "text/html; charset=utf-8"
      }
    })
  }

  none() {
    return this.resp(null, {
      headers: {
        "Content-Length": "0"
      }
    })
  }

  private resp(body: any, options?: RespOptions) {
    return new Response(JSON.stringify(body), {
      status: this.status,
      headers: {...this.headers, ...options?.headers}
    })
  }
}